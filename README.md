[![pipeline status](https://gitlab.com/openstreetcraft/sidecar/badges/master/pipeline.svg)](https://gitlab.com/openstreetcraft/sidecar/commits/master)
[![Codacy Badge](https://app.codacy.com/project/badge/Grade/900151676edf4d50b16147a08945ccdc)](https://www.codacy.com/gl/openstreetcraft/sidecar/dashboard?utm_source=gitlab.com&amp;utm_medium=referral&amp;utm_content=openstreetcraft/sidecar&amp;utm_campaign=Badge_Grade)
[![Javadoc Badge](https://img.shields.io/badge/javadoc-brightgreen.svg)](https://openstreetcraft.gitlab.io/sidecar/docs/javadoc/)

# Integrate Any Webapp Into Spring Cloud Using Sidecar Applications #

```
overpass:
  image: registry.gitlab.com/openstreetcraft/sidecar
  environment:
  - SPRING_APPLICATION_NAME=overpass
  - SIDECAR_HOSTNAME=overpass-api.de
  - SIDECAR_PORT=80
  - EUREKA_INSTANCE_HOSTNAME=overpass-api.de
  links:
  - eureka
```
