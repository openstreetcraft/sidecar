# How to contribute

Openstreetcraft is an open source project and we welcome contributions from the community.

To get started, read these [contribution guidelines](https://openstreetcraft.gitlab.io/contributing/).
